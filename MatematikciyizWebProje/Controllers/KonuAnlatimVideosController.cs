﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MatematikciyizWebProje.Models;

namespace MatematikciyizWebProje.Controllers
{
    public class KonuAnlatimVideosController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: KonuAnlatimVideos
        public async Task<ActionResult> Index()
        {
            var konuAnlatimVideos = db.KonuAnlatimVideos.Include(k => k.KonuAnlatim);
            return View(await konuAnlatimVideos.ToListAsync());
        }

        // GET: KonuAnlatimVideos/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            KonuAnlatimVideo konuAnlatimVideo = await db.KonuAnlatimVideos.FindAsync(id);
            if (konuAnlatimVideo == null)
            {
                return HttpNotFound();
            }
            return View(konuAnlatimVideo);
        }

        // GET: KonuAnlatimVideos/Create
        public ActionResult Create()
        {
            ViewBag.KonuID = new SelectList(db.KonuAnlatims, "KonuAnlatimID", "KonuAdi");
            return View();
        }

        // POST: KonuAnlatimVideos/Create
        // Aşırı gönderim saldırılarından korunmak için, lütfen bağlamak istediğiniz belirli özellikleri etkinleştirin, 
        // daha fazla bilgi için https://go.microsoft.com/fwlink/?LinkId=317598 sayfasına bakın.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "KonuVideoID,Link,KonuID")] KonuAnlatimVideo konuAnlatimVideo)
        {
            if (ModelState.IsValid)
            {
                db.KonuAnlatimVideos.Add(konuAnlatimVideo);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.KonuID = new SelectList(db.KonuAnlatims, "KonuAnlatimID", "KonuAdi", konuAnlatimVideo.KonuID);
            return View(konuAnlatimVideo);
        }

        // GET: KonuAnlatimVideos/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            KonuAnlatimVideo konuAnlatimVideo = await db.KonuAnlatimVideos.FindAsync(id);
            if (konuAnlatimVideo == null)
            {
                return HttpNotFound();
            }
            ViewBag.KonuID = new SelectList(db.KonuAnlatims, "KonuAnlatimID", "KonuAdi", konuAnlatimVideo.KonuID);
            return View(konuAnlatimVideo);
        }

        // POST: KonuAnlatimVideos/Edit/5
        // Aşırı gönderim saldırılarından korunmak için, lütfen bağlamak istediğiniz belirli özellikleri etkinleştirin, 
        // daha fazla bilgi için https://go.microsoft.com/fwlink/?LinkId=317598 sayfasına bakın.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "KonuVideoID,Link,KonuID")] KonuAnlatimVideo konuAnlatimVideo)
        {
            if (ModelState.IsValid)
            {
                db.Entry(konuAnlatimVideo).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.KonuID = new SelectList(db.KonuAnlatims, "KonuAnlatimID", "KonuAdi", konuAnlatimVideo.KonuID);
            return View(konuAnlatimVideo);
        }

        // GET: KonuAnlatimVideos/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            KonuAnlatimVideo konuAnlatimVideo = await db.KonuAnlatimVideos.FindAsync(id);
            if (konuAnlatimVideo == null)
            {
                return HttpNotFound();
            }
            return View(konuAnlatimVideo);
        }

        // POST: KonuAnlatimVideos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            KonuAnlatimVideo konuAnlatimVideo = await db.KonuAnlatimVideos.FindAsync(id);
            db.KonuAnlatimVideos.Remove(konuAnlatimVideo);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
