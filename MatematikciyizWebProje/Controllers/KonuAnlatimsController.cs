﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MatematikciyizWebProje.Models;

namespace MatematikciyizWebProje.Controllers
{
    public class KonuAnlatimsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: KonuAnlatims
        public async Task<ActionResult> Index()
        {
            var konuAnlatims = db.KonuAnlatims.Include(k => k.Dersler);
            return View(await konuAnlatims.ToListAsync());
        }

        // GET: KonuAnlatims/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            KonuAnlatim konuAnlatim = await db.KonuAnlatims.FindAsync(id);
            if (konuAnlatim == null)
            {
                return HttpNotFound();
            }
            return View(konuAnlatim);
        }

        // GET: KonuAnlatims/Create
        public ActionResult Create()
        {
            ViewBag.DersID = new SelectList(db.Derslers, "DersID", "DersAdi");
            return View();
        }

        // POST: KonuAnlatims/Create
        // Aşırı gönderim saldırılarından korunmak için, lütfen bağlamak istediğiniz belirli özellikleri etkinleştirin, 
        // daha fazla bilgi için https://go.microsoft.com/fwlink/?LinkId=317598 sayfasına bakın.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "KonuAnlatimID,KonuAdi,DersID")] KonuAnlatim konuAnlatim)
        {
            if (ModelState.IsValid)
            {
                db.KonuAnlatims.Add(konuAnlatim);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.DersID = new SelectList(db.Derslers, "DersID", "DersAdi", konuAnlatim.DersID);
            return View(konuAnlatim);
        }

        // GET: KonuAnlatims/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            KonuAnlatim konuAnlatim = await db.KonuAnlatims.FindAsync(id);
            if (konuAnlatim == null)
            {
                return HttpNotFound();
            }
            ViewBag.DersID = new SelectList(db.Derslers, "DersID", "DersAdi", konuAnlatim.DersID);
            return View(konuAnlatim);
        }

        // POST: KonuAnlatims/Edit/5
        // Aşırı gönderim saldırılarından korunmak için, lütfen bağlamak istediğiniz belirli özellikleri etkinleştirin, 
        // daha fazla bilgi için https://go.microsoft.com/fwlink/?LinkId=317598 sayfasına bakın.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "KonuAnlatimID,KonuAdi,DersID")] KonuAnlatim konuAnlatim)
        {
            if (ModelState.IsValid)
            {
                db.Entry(konuAnlatim).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.DersID = new SelectList(db.Derslers, "DersID", "DersAdi", konuAnlatim.DersID);
            return View(konuAnlatim);
        }

        // GET: KonuAnlatims/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            KonuAnlatim konuAnlatim = await db.KonuAnlatims.FindAsync(id);
            if (konuAnlatim == null)
            {
                return HttpNotFound();
            }
            return View(konuAnlatim);
        }

        // POST: KonuAnlatims/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            KonuAnlatim konuAnlatim = await db.KonuAnlatims.FindAsync(id);
            db.KonuAnlatims.Remove(konuAnlatim);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
