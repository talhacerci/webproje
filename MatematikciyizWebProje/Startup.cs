﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MatematikciyizWebProje.Startup))]
namespace MatematikciyizWebProje
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
