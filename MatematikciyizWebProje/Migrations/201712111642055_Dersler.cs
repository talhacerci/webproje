namespace MatematikciyizWebProje.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Dersler : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.KonuAnlatim", "Dersler_DersID", "dbo.Dersler");
            DropIndex("dbo.KonuAnlatim", new[] { "Dersler_DersID" });
            DropColumn("dbo.KonuAnlatim", "Dersler_DersID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.KonuAnlatim", "Dersler_DersID", c => c.Int());
            CreateIndex("dbo.KonuAnlatim", "Dersler_DersID");
            AddForeignKey("dbo.KonuAnlatim", "Dersler_DersID", "dbo.Dersler", "DersID");
        }
    }
}
