namespace MatematikciyizWebProje.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class KonuAnlatim : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Dersler",
                c => new
                    {
                        DersID = c.Int(nullable: false, identity: true),
                        DersAdi = c.String(nullable: false, maxLength: 200),
                    })
                .PrimaryKey(t => t.DersID);
            
            CreateTable(
                "dbo.KonuAnlatim",
                c => new
                    {
                        KonuAnlatimID = c.Int(nullable: false, identity: true),
                        KonuAdi = c.String(nullable: false, maxLength: 200),
                        Dersler_DersID = c.Int(),
                    })
                .PrimaryKey(t => t.KonuAnlatimID)
                .ForeignKey("dbo.Dersler", t => t.Dersler_DersID)
                .Index(t => t.Dersler_DersID);
            
            CreateTable(
                "dbo.KonuAnlatimVideo",
                c => new
                    {
                        KonuVideoID = c.Int(nullable: false, identity: true),
                        Link = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.KonuVideoID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.KonuAnlatim", "Dersler_DersID", "dbo.Dersler");
            DropIndex("dbo.KonuAnlatim", new[] { "Dersler_DersID" });
            DropTable("dbo.KonuAnlatimVideo");
            DropTable("dbo.KonuAnlatim");
            DropTable("dbo.Dersler");
        }
    }
}
