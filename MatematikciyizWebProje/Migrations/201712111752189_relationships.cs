namespace MatematikciyizWebProje.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class relationships : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.KonuAnlatim", "DersID", c => c.Int(nullable: false));
            AddColumn("dbo.KonuAnlatimVideo", "KonuID", c => c.Int(nullable: false));
            CreateIndex("dbo.KonuAnlatim", "DersID");
            CreateIndex("dbo.KonuAnlatimVideo", "KonuID");
            AddForeignKey("dbo.KonuAnlatim", "DersID", "dbo.Dersler", "DersID", cascadeDelete: true);
            AddForeignKey("dbo.KonuAnlatimVideo", "KonuID", "dbo.KonuAnlatim", "KonuAnlatimID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.KonuAnlatimVideo", "KonuID", "dbo.KonuAnlatim");
            DropForeignKey("dbo.KonuAnlatim", "DersID", "dbo.Dersler");
            DropIndex("dbo.KonuAnlatimVideo", new[] { "KonuID" });
            DropIndex("dbo.KonuAnlatim", new[] { "DersID" });
            DropColumn("dbo.KonuAnlatimVideo", "KonuID");
            DropColumn("dbo.KonuAnlatim", "DersID");
        }
    }
}
