﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MatematikciyizWebProje.Models
{
    [Table("KonuAnlatimVideo")]
    public class KonuAnlatimVideo
    {
        [Key]
        public int KonuVideoID { get; set; }
        [Required]
        [AllowHtml]
        public string Link { get; set; }

        public int KonuID { get; set; }
        [ForeignKey("KonuID")]
        public KonuAnlatim KonuAnlatim { get; set; }

    }
}