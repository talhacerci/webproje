﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MatematikciyizWebProje.Models
{
    [Table("KonuAnlatim")]
    public class KonuAnlatim
    { 
    [Key]
    public int KonuAnlatimID { get; set; }
    [Required, MaxLength(200)]
    public string KonuAdi { get; set; }
        public int DersID { get; set; }
        [ForeignKey("DersID")]
        public Dersler Dersler { get; set; }
        public virtual List<KonuAnlatimVideo> KonuAnlatimVideos { set; get; }
    }
}